const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  title: "LiveCode.RVA",
  description: "",

  dest: "./public/",

  themeConfig: {},

  markdown: {
    anchor: {
      permalink: true,
      permalinkBefore: false,
      permalinkSymbol: ""
    }
  },

  configureWebpack: (config, isServer) => {
    return {
      plugins: [new CopyWebpackPlugin([{ from: "image", to: "image" }])]
    };
  }
};
